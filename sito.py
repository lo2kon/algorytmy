n = 100
a = [0]*n

k = 2
while k*k<=n:
    if not a[k]:
        for i in range(k*k,n,k):
            a[i] = 1
    k+=1
    
for i in range(2,n):
    if not a[i]: print(i)